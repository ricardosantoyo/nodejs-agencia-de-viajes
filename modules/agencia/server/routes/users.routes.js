'use strict';
/*
    Modulos de dependencia
*/
var express = require('express');
var router = express.Router();
var agencia = require('../controllers/mainController');

router.route('/hoteles').get(agencia.getHotels);
router.route('/hoteles/:id').get(agencia.getHotelRes);
router.route('/viajes').get(agencia.getTravels);
router.route('/viajes/:id').get(agencia.getTravelRes);
router.route('/carros').get(agencia.getCars);
router.route('/carros/:id').get(agencia.getCarRes);
router.route('/login/:email/:pass').get(agencia.signin);

module.exports = router;

//test route to make sure everything is working (accessed at GET http://localhost:8080/api)
/*router.get('/', function(req, res){
  res.json({
    id: 12345,
    content: "Hello, World!"
  });
});

router.get('/hoteles', function(req, res){
  res.json([{
    nombre: "Marriot",
    precio: "$5,000.00",
    estrellas: "5",
    ciudad: "Aguascalientes"
  },
  {
    nombre: "City Express",
    precio: "$1,000.00",
    estrellas: "3",
    ciudad: "Zacatecas"
  },
  {
    nombre: "City Express",
    precio: "$1,000.00",
    estrellas: "3",
    ciudad: "Zacatecas"
  },
  {
    nombre: "City Express",
    precio: "$1,000.00",
    estrellas: "3",
    ciudad: "Zacatecas"
  },
  {
    nombre: "City Express",
    precio: "$1,000.00",
    estrellas: "3",
    ciudad: "Zacatecas"
  }]);
});

router.get('/viajes', function(req, res){
  res.json([{
    desde: "San Diego",
    hasta: "San Francisco",
    precio: "$3,500.00"
  },
  {
    desde: "Aguascalientes",
    hasta: "Paris",
    precio: "$14,500.00"
  }]);
});

router.get('/carros', function(req, res){
  res.json([{
    marca: "Nissan",
    modelo: "Versa 2015",
    precio: "$200.00",
    ciudad: "Aguascalientes"
  },
  {
    marca: "Chevrolet",
    modelo: "Camaro 2016",
    precio: "$1,000.00",
    ciudad: "Zacatecas"
  }]);
});

router.get('/viajes/:id', function(req, res){
  console.log("Viajes del usuario "+req.params.id);
  res.json({
    desde: "San Diego",
    hasta: "San Francisco",
    precio: "$3,500.00"
  });
});

router.get('/hoteles/:id', function(req, res){
  console.log("Hoteles del usuario "+req.params.id);
  res.json({
    nombre: "Marriot",
    precio: "$5,000.00",
    estrellas: "5",
    ciudad: "Aguascalientes"
  });
});

router.get('/carros/:id', function(req, res){
  console.log("Carros del usuario "+req.params.id);
  res.json({
    marca: "Mazda",
    modelo: "Mazda 3 2015",
    precio: "$350.00",
    ciudad: "Aguascalientes"
  });
});

router.get('/login/:email/:pass', function(req, res){
  if(req.params.email == "foo@example.com"){
    if(req.params.pass == "12345"){
      res.json({
        ok: true,
        email: true,
        nombre: "Ricardo",
        apellidos: "Santoyo Reyes",
        id: 151607
      });
    } else {
      res.json({
        ok: false,
        email: true
      });
    }
  } else {
    res.json({
      ok: false,
      email: false
    });
  }
});*/
