var mysql = require('mysql');

function Connection() { // Se define la conexion a MySQL con el host, usuario, contrasena y nombre de la BD a utilizar. A su vez, se tiene un limite de coneccion.
  this.pool = null;

  this.init = function() {
    this.pool = mysql.createPool({
      host: 'localhost',
      user: 'root',
      password: 'aerithwhite24',
      database: 'agencia',
      connectionLimit: 100
    });
  };

  this.acquire = function(callback) {
    this.pool.getConnection(function(err, connection) { // se manda la conexion para utilizar MySQL.
      callback(err, connection);
    });
  };
}

module.exports = new Connection();
