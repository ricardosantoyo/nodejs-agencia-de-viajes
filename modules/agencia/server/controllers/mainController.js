var connection = require('../config/connection'); // Se manda llamar a la coneccion con MySQL.
var connection2 = require('../config/connection2');

exports.getHotels = function(request, response){
    connection.acquire(function(err, con) {
      con.query('select * from hotel_empresa', function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
            if (result.length==0){
                response.status(404);
                return response.json({err: 'No se encontraron registros de empresas en el sistema.'});
            } else{
                response.status(200);
                return response.json(result);
            }
        }
      });
    });
}

exports.getHotelRes = function(request, response){
    connection.acquire(function(err, con) {
      con.query('select empresa.Nombre as nombre, hotel.Precio as precio, hotel.Estrellas as estrellas, ciudad.Nombre as ciudad from hotel_usuario, hotel, empresa, ciudad where hotel_usuario.IDUsuario = ? and hotel.ID = hotel_usuario.IDHotel and hotel.IDEmpresa = empresa.ID and hotel.IDCiudad = ciudad.ID', request.params.id, function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
            if (result.length==0){
              connection2.acquire(function(err, con2) {
                con2.query('select empresa.Nombre as nombre, hotel.Precio as precio, hotel.Estrellas as estrellas, ciudad.Nombre as ciudad from hotel_usuario, hotel, empresa, ciudad where hotel_usuario.IDUsuario = ? and hotel.ID = hotel_usuario.IDHotel and hotel.IDEmpresa = empresa.ID and hotel.IDCiudad = ciudad.ID', request.params.id, function(err, result2){
                  con2.release();
                  if (err){
                    response.status(400);
                    return response.json({err: err});
                  } else{
                      if (result2.length==0){
                          response.status(404);
                          return response.json({err: 'No se encontraron registros de hoteles para el usuario en el sistema.'});
                      } else{
                          response.status(200);
                          return response.json(result2);
                      }
                  }
                });
              });
            } else{
                response.status(200);
                return response.json(result);
            }
        }
      });
    });
}

exports.getTravels = function(request, response){
    connection.acquire(function(err, con) {
        con.query('select * from viaje_empresa', function(err, result){
          con.release();
          if (err){
            response.status(400);
            return response.json({err: err});
          } else{
              if (result.length==0){
                  response.status(404);
                  return response.json({err: 'No se encontraron registros de viajes en el sistema.'});
              } else{
                  //console.log(result[0].Origen);
                  var j=0, k=0;
                  var send = false;
                  for (i=0;i<result.length;i++){
                      con.query('select Nombre from ciudad where ID = ?', result[i].Origen, function(err, result2){
                          if (err){
                              response.status(400);
                              return response.json({err: err});
                          } else{
                              //console.log(result[j]);
                              result[j].Origen = result2[0].Nombre;
                              //console.log(result[j].Origen);
                              j++;
                              if ((k==i && j==k) && !send){
                                  send = true;
                                  response.status(200);
                                  return response.json(result);
                              }
                          }
                      });
                      con.query('select Nombre from ciudad where ID = ?', result[i].Destino, function(err, result2){
                          if (err){
                              response.status(400);
                              return response.json({err: err});
                          } else{
                              //console.log(result[j]);
                              result[k].Destino = result2[0].Nombre;
                              //console.log(result[k].Destino);
                              k++;
                              if ((k==i && j==k) && !send){
                                  send = true;
                                  response.status(200);
                                  return response.json(result);
                              }
                          }
                      });
                  }
              }
          }
        });
    });
}

exports.getTravelRes = function(request, response){
    connection.acquire(function(err, con) {
        con.query('select viaje.Origen as Origen, viaje.Destino as Destino, viaje.precio as Precio, viaje.Cupo as Cupo, empresa.Nombre as Nombre from viaje_usuario, viaje, empresa where viaje_usuario.IDUsuario = ? and viaje.ID = viaje_usuario.IDViaje and viaje.IDEmpresa = empresa.ID', request.params.id, function(err, result){
          con.release();
          if (err){
            response.status(400);
            return response.json({err: err});
          } else{
              if (result.length==0){
                response.status(404);
                return response.json({err: 'No se encontraron registros de viajes para el usuario en el sistema.'});
              } else{
                  var j=0, k=0;
                  var send = false;
                  connection2.acquire(function(err, con2) {
                    con2.query('select Nombre from ciudad where ID = ?', result[0].Origen, function(err, result2){
                      con2.release();
                        if (err){
                            response.status(400);
                            return response.json({err: err});
                        } else{
                            //console.log(result[j]);
                            result[j].Origen = result2[0].Nombre;
                            //console.log(result[j].Origen);
                            j++;
                            if ((k==1 && j==1) && !send){
                                send = true;
                                response.status(200);
                                return response.json(result);
                            }
                        }
                    });
                    con2.query('select Nombre from ciudad where ID = ?', result[0].Destino, function(err, result2){
                        if (err){
                            response.status(400);
                            return response.json({err: err});
                        } else{
                            //console.log(result[j]);
                            result[k].Destino = result2[0].Nombre;
                            //console.log(result[k].Destino);
                            k++;
                            if ((k==1 && j==1) && !send){
                                send = true;
                                response.status(200);
                                return response.json(result);
                            }
                        }
                    });
                  }
              }
          }
        });
    });
}

exports.getCars = function(request, response){
    connection.acquire(function(err, con) {
      con.query('select * from carro_empresa', function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
            if (result.length==0){
                response.status(404);
                return response.json({err: 'No se encontraron registros de carros en el sistema.'});
            } else{
                response.status(200);
                return response.json(result);
            }
        }
      });
    });
}

exports.getCarRes = function(request, response){
    connection.acquire(function(err, con) {
      con.query("select CONCAT(carro.Marca, ' - ', carro.Modelo) as Titulo, CONCAT('Cilindros: ', carro.Cilindros, ', Gasolina: ', carro.Gasolina, ', Puertas: ', carro.Puertas, ', Asientos: ', carro.Asientos) as Descripcion, renta_carro.Precio as Precio, ciudad.Nombre as Ciudad from renta_carro, carro, ciudad, carro_usuario where carro_usuario.IDUsuario = ? and carro_usuario.IDRentaCarro = renta_carro.ID and renta_carro.IDCarro = carro.ID and renta_carro.IDCiudad = ciudad.ID", request.params.id, function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
            if (result.length==0){
              connection2.acquire(function(err, con2) {
                con2.query("select CONCAT(carro.Marca, ' - ', carro.Modelo) as Titulo, CONCAT('Cilindros: ', carro.Cilindros, ', Gasolina: ', carro.Gasolina, ', Puertas: ', carro.Puertas, ', Asientos: ', carro.Asientos) as Descripcion, renta_carro.Precio as Precio, ciudad.Nombre as Ciudad from renta_carro, carro, ciudad, carro_usuario where carro_usuario.IDUsuario = ? and carro_usuario.IDRentaCarro = renta_carro.ID and renta_carro.IDCarro = carro.ID and renta_carro.IDCiudad = ciudad.ID", request.params.id, function(err, result2){
                  con2.release();
                  if (err){
                    response.status(400);
                    return response.json({err: err});
                  } else{
                      if (result2.length==0){
                          response.status(404);
                          return response.json({err: 'No se encontraron registros de carros para el usuario en el sistema.'});
                      } else{
                          response.status(200);
                          return response.json(result2);
                      }
                  }
                });
              });
            } else{
                response.status(200);
                return response.json(result);
            }
        }
      });
    });
}

exports.signin = function(request, response){
    connection.acquire(function(err, con) {
      con.query("select Nombre as nombre, CONCAT(ApellidoPat, ' ', ApellidoMat) as apellidos, ID as id from usuario where Email = ? and Contraseña = md5( ? )", [request.params.email, request.params.pass], function(err, result){
        con.release();
        if (err){
          response.status(400);
          return response.json({err: err});
        } else{
            if (result.length==0){
                response.status(404);
                return response.json({ok: 'false', email: 'false'});
            } else{
                response.status(200);
                return response.json({ok: 'true', email: 'true', data: result});
            }
        }
      });
    });
}
