var express = require('express');
var bodyparser = require('body-parser');
var connection = require('./modules/agencia/server/config/connection');
var connection2 = require('./modules/agencia/server/config/connection2');
var routes = require('./modules/agencia/server/routes/users.routes');
var app = express();

connection.init();
connection2.init(); // se inicia la coneccion a las BDs.
//configure app to use bodyParser()
//this will let us get the data from a POST
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json()); // Maneja JSON en el body del request.
app.use(express.static(__dirname + '/modules/agencia/client'));
app.use('/api', routes);

var port = process.env.PORT || 3000; //set our port

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Servidor iniciado en puerto ' + port);
